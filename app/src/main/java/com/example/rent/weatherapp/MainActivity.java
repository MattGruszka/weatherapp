package com.example.rent.weatherapp;

import android.os.AsyncTask;
import android.os.NetworkOnMainThreadException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    HttpURLConnection urlConnection;
    String response;
    Button buttonTemp;
    String humidity;
    @BindView(R.id.editCity)
    EditText editCity;
    ArrayList array;
    @BindView(R.id.putTemp)
    TextView tempText;
    @BindView(R.id.putHumi)
    TextView humiText;
    @BindView(R.id.putWind)
    TextView windText;
    @BindView(R.id.putCloud)
    TextView cloudText;
    @BindView(R.id.putDescription)
    TextView descriptionText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
    }
    @OnClick(R.id.buttonCheck)
    public void checkWeather(){
        if(editCity.getText().toString().equals("")) {
            Toast.makeText(this, "Please, put city name.", Toast.LENGTH_SHORT).show();
        } else {
            new Gdata().execute();
        }
    }


    public ArrayList Connect() {
        try {
            String city = editCity.getText().toString();
            array = new ArrayList();
            URL url = new URL("http://api.openweathermap.org/data/2.5/weather?q="+city+"&appid=65b2cefe96066a833ed493d04d6a2c78&units=metric");
            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
            StringBuilder textBuilder = new StringBuilder();
            try (Reader reader = new BufferedReader(new InputStreamReader(inputStream,
                    Charset.forName(StandardCharsets.UTF_8.name())))) {
                int c;
                while ((c = reader.read()) != -1) {
                    textBuilder.append((char) c);
                }
            }
            response = textBuilder.toString();
            JSONObject json = new JSONObject(response);

            JSONObject subMain = json.getJSONObject("main");
            String temperatura = subMain.getString("temp");
            array.add(temperatura);
            String humidity = subMain.getString("humidity");
            array.add(humidity);

            JSONObject subWind = json.getJSONObject("wind");
            String wind = subWind.getString("speed");
            array.add(wind);

            JSONObject subRain = json.getJSONObject("clouds");
            String clouds = subRain.getString("all");
            array.add(clouds);

            JSONArray subDesc = json.getJSONArray("weather");
            for (int i=0; i<subDesc.length(); i++ ) {
                JSONObject jsonObj = subDesc.getJSONObject(i);
                String description = jsonObj.getString("description");
                array.add(description);

            }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }
        return array;
    }


    private class Gdata extends AsyncTask<ArrayList, Void, ArrayList> {

        @Override
        protected ArrayList doInBackground(ArrayList... arrayList) {

            ArrayList arrayData = MainActivity.this.Connect();
            return arrayData;
        }
        @Override
        protected void onPostExecute(ArrayList arrayData) {
            tempText.setText(arrayData.get(0).toString());
            humiText.setText(arrayData.get(1).toString());
            windText.setText(arrayData.get(2).toString());
            cloudText.setText(arrayData.get(3).toString());
            descriptionText.setText(arrayData.get(4).toString());
        }
    }
}